import json 

from kafka import KafkaConsumer


consumer = KafkaConsumer(
                                "registered_user",
                                bootstrap_servers = '13.233.136.253:9092',
                                auto_offset_reset = 'earliest',
                                group_id = 'consumer-group-A'
                                )

def get_messages():
    
    print("Starting the consumer")
    for message in consumer:
        print("register_user=",(json.loads(message.value)))

get_messages()